#include "Tile.h"

std::string FromFeatureToString(Feature feature)
{
	std::string stringFeature;

	if (feature == Feature::Road)
	{
		stringFeature = "Road";
	}
	else
		if (feature == Feature::Cloister)
		{
			stringFeature = "Cloister";
		}
		else
			if (feature == Feature::City)
			{
				stringFeature = "City";
			}
			else
				if (feature == Feature::Field)
				{
					stringFeature = "Field";
				}
				else
					if (feature == Feature::RoadCross)
					{
						stringFeature = "RoadCross";
					}

	return stringFeature;
}

Tile::Tile(Feature up, Feature right, Feature middle, Feature bottom, Feature left, std::string name) :
	m_up(up),
	m_right(right),
	m_middle(middle),
	m_bottom(bottom),
	m_left(left),
	m_name(name)
{
}

Tile::Tile(const Tile & otherTile)
{
	this->m_up = otherTile.m_up;
	this->m_right = otherTile.m_right;
	this->m_middle = otherTile.m_middle;
	this->m_bottom = otherTile.m_bottom;
	this->m_left = otherTile.m_left;
	this->m_name = otherTile.m_name;
}

Tile::Tile(Tile&& otherTile)
{
	*this = std::move(otherTile);
}

Tile & Tile::operator=(Tile & otherTile)
{
	this->m_up = otherTile.m_up;
	this->m_right = otherTile.m_right;
	this->m_middle = otherTile.m_middle;
	this->m_bottom = otherTile.m_bottom;
	this->m_left = otherTile.m_left;
	this->m_name = otherTile.m_name;

	return *this;
}

Feature Tile::GetUp() const
{
	return m_up;
}

Feature Tile::GetRight() const
{
	return m_right;
}

Feature Tile::GetMiddle() const
{
	return m_middle;
}

Feature Tile::GetDown() const
{
	return m_bottom;
}

Feature Tile::GetLeft() const
{
	return m_left;
}

std::string Tile::GetName() const
{
	return m_name;
}

void Tile::Rotation()
{
	Feature aux = m_up;
	m_up = m_right;
	m_right = m_bottom;
	m_bottom = m_left;
	m_left = aux;
}

bool Tile::hasFollower(Side side)
{
	//if (m_upFollower != NULL || m_rightFollower != NULL || m_bottomFollower != NULL || m_leftFollower != NULL)
	//	return true;
	//else
		return false;
}

Tile& Tile::operator = (const Tile& otherTile)
{
	this->m_up = otherTile.m_up;
	this->m_right = otherTile.m_right;
	this->m_middle = otherTile.m_middle;
	this->m_bottom = otherTile.m_bottom;
	this->m_left = otherTile.m_left;
	this->m_name = otherTile.m_name;

	return *this;
}

Tile& Tile::operator = (Tile&& otherTile)
{
	this->m_up = otherTile.m_up;
	this->m_right = otherTile.m_right;
	this->m_middle = otherTile.m_middle;
	this->m_bottom = otherTile.m_bottom;
	this->m_left = otherTile.m_left;
	this->m_name = otherTile.m_name;

	new(&otherTile) Tile;

	return *this;

}

std::ostream& operator <<(std::ostream& outputStream, const Tile& tile)
{
	std::string upSide = FromFeatureToString(tile.m_up);
	std::string rightSide = FromFeatureToString(tile.m_right);
	std::string middleSide = FromFeatureToString(tile.m_middle);
	std::string bottomSide = FromFeatureToString(tile.m_bottom);
	std::string leftSide = FromFeatureToString(tile.m_left);

	return outputStream << "up: " << upSide << ", right: " << rightSide << ", middle: "<< middleSide<< ", bottom: " 
		<< bottomSide << ", left: " << leftSide <<std::endl;
}






