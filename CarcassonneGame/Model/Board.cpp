#include "Board.h"

Board::Board(Tile &startTile)
{
	m_maxBottom = 9;
	m_maxRight = 9;

	m_board.resize(m_maxBottom);

	for (int row = 1; row < m_maxBottom; row++)
	{
		m_board[row].resize(m_maxRight);
	}

	m_board[1][1] = &startTile;
}

void Board::PushDownLines()
{
	m_maxBottom++;

	m_board.resize(m_maxBottom);

	for (int ind = m_maxBottom; ind >= 1; ind--)
	{
		m_board[ind] = m_board[ind - 1];
	}
}

void Board::PushRightColumns()
{
	m_maxRight++;

	for (int row = 1; row < m_maxBottom; row++)
	{
		m_board[row].resize(m_board[row].size() + 1);

		for (int col = m_board[row].capacity(); col >= 1; col--)
		{
			m_board[row][col] = m_board[row][col - 1];
		}
	}

}

void Board::AddTile(const int& x, const int& y, Tile * tile)
{
	if (IsValidPosition(x, y, tile))
	{

		if (x == 0)
		{
			PushDownLines();

			m_board[1][y] = tile;
		}
		else
			if (y == 0)
			{
				PushRightColumns();

				m_board[x][1] = tile;
			}
			else
			{
				m_board[x][y] = tile;
			}

	}
	//m_numberOfTiles++;
}

bool Board::IsFreePosition(int x,int y)
{
	bool insideHoriziontally = x >= 0 && x <= m_maxRight;
	bool insideVertically = y >= 0 && y <= m_maxBottom;
	if (!insideHoriziontally && !insideVertically)
	{
		return false;
	}
	bool freeLocation = m_board[x][y] == NULL;
	return freeLocation;
}

bool Board::IsValidPosition(const int& x, const int& y, Tile * tile)
{
	bool ok = false;
	if (IsFreePosition(x, y))
		if (!IsFreePosition(x - 1, y) && m_board[x - 1][y]->GetDown() != tile->GetUp())
			return false;
		else
			if (!IsFreePosition(x + 1, y) && m_board[x + 1][y]->GetUp() != tile->GetDown())
				return false;
			else
				if (!IsFreePosition(x, y - 1) && m_board[x][y - 1]->GetRight() != tile->GetLeft())
					return false;
				else
					if (!IsFreePosition(x, y + 1) && m_board[x][y + 1]->GetLeft() != tile->GetRight())
						return false;
					else
						return true;
}

void Board::possiblePositions(Tile * tile)
{

	std::vector<std::pair<int, int >> validPostions;
	int count = 0;
	for (int row = 0; row < m_maxBottom; row++)
		for (int col = 0; col < m_maxRight; col++)
			for (int rotations = 0; rotations < 4; rotations++)
				if (IsValidPosition(row, col, tile)) {
					validPostions[count++].first = row;
					validPostions[count++].first = col;
					break;
				}
				else
					tile->Rotation();

	
}









