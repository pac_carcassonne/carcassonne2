#include "Player.h"
#include "Follower.h"

#include <string>

Player::Color Player::FromStringToColor(const std::string& stringColor)
{
		Player::Color color = Player::Color::None;

		if (stringColor == "Red")
		{
			color = Player::Color::Red;
		}
		else
			if (stringColor == "Green")
			{
				color = Player::Color::Black;
			}
			else
				if (stringColor == "Blue")
				{
					color = Player::Color::Blue;
				}
				else
					if (stringColor == "Yellow")
					{
						color = Player::Color::Yellow;
					}
					else
						if (stringColor == "Black")
						{
							color = Player::Color::Black;
						}
						else
							throw "pick other color: ";

		return color;
}


Player::Player(const std::string& name) :
	m_name(name)
{
}

std::string Player::GetPlayerName()
{
	return m_name;
}

//Tile Player::PickUnusedTile(UnusedTiles & unusedTile) const
//{
//	UnusedTiles chosenTile;
//
//}

bool Player::AvailableFollowers()
{
	std::list<Follower> followers;

	//if()

	return false;
}

bool Player::AvailableTiles()
{
	return false;
}

bool Player::operator==(const Player & firstOther)
{
	if (this == const_cast<Player*>(&firstOther))
	{
		return true;
	}

	return false;
}

void Player::SetColor(std::string color)
{
	playerColor = FromStringToColor(color);
}

void Player::SetFollowers(const int & numberOfFollowers)
{
	for (int i = 0; i < numberOfFollowers; ++i)
	{
		Follower follower;
		m_followers.push_back(follower);
	}
}

Player::Color Player::GetColor() const
{
	return playerColor;
}








