#pragma once

#include "Tile.h"
#include "Follower.h"
#include "UnusedTiles.h"

#include <string>
#include <iostream>
#include <vector>
#include <list>

class Player
{
public:
	enum class Color
	{
		Red,
		Green,
		Blue,
		Yellow,
		Black,
		None
	};

	Player(const std::string& name);

	std::string GetPlayerName();

	Tile PickUnusedTile(UnusedTiles& unusedTile) const;

	bool AvailableFollowers();

	bool AvailableTiles();

	bool operator == (const Player& firstOther);

	Player::Color FromStringToColor(const std::string& stringColor);

	Player::Color GetColor() const;

	void SetColor(std::string color);

	void SetFollowers(const int& numberOfFollowers);

private:
	std::string m_name;
	Color playerColor;
	Color m_Color;
	std::vector <Follower> m_followers;
};

