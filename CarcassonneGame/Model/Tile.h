#pragma once

#include"Side.h"
#include"Feature.h"

#include <string>

class Tile
{
public:
	Tile(Feature up = Feature::Empty, Feature right = Feature::Empty, Feature middle = Feature::Empty,
		Feature bottom = Feature::Empty, Feature left = Feature::Empty, std::string name = nullptr);
	
	Tile(const Tile& otherTile);
	
	Tile(Tile&& otherTile);

	Tile& operator = (Tile& otherTile);

	Feature GetUp() const;

	Feature GetRight() const;

	Feature GetMiddle() const;

	Feature GetDown() const;

	Feature GetLeft() const;

	std::string GetName() const;

	void Rotation();

	bool hasFollower(Side side);

	Tile& operator = (const Tile& otherTile);

	Tile& operator = (Tile&& otherTile);

	friend std::ostream& operator <<(std::ostream& outputStream, const Tile& tile);

private:
	Feature m_up;
	Feature m_right;
	Feature m_middle;
	Feature m_bottom;
	Feature m_left;
	std::string m_name;
	Follower m_upFollower();
	Follower m_rightFollower();
	Follower m_bottomFollower();
	Follower m_leftFollower();
};

