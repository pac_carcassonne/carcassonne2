#include "ScoreTable.h"

void ScoreTable::UpdatePlayerScore(const Player& currentPlayer, const int& numberOfPoints)
{
	for (unsigned index = 0; index < m_playersScore.size(); ++index)
	{
		if (m_playersScore[index].first == currentPlayer)
		{
			m_playersScore[index].second += numberOfPoints;
		}
	}
}

int ScoreTable::GetNumberOfPlayers()
{
	return m_playersScore.size();
}

void ScoreTable::AddPlayer(const Player & newPlayer)
{
	m_playersScore.push_back(std::make_pair(newPlayer, 0));
}
