#include "Follower.h"

Follower::Follower()
{
}

Follower::Follower(Color color, State position, int& lineIndex, int& columnIndex) :
	m_color(color),
	m_state(position)
{
	m_position.first = lineIndex;
	m_position.second = columnIndex;
}

void Follower::Remove()
{
}

Color Follower::GetColor()
{
	return m_color;
}

State Follower::GetState()
{
	return m_state;
}
