#pragma once

enum class Color
{
	Black,
	Red,
	Yellow,
	Blue,
	Green
};

enum class State
{
	Vertical,
	Orizontal
};
