#pragma once

#include "FollowerType.h"

#include <utility>

class Follower
{
public:
	Follower();

	Follower(Color color, State state, int& lineIndex, int& columnIndex);

	~Follower() = default;

	void Remove();

	Color GetColor();

	State GetState();

private:
	Color m_color;
	State m_state;
	std::pair<int, int> m_position;
};

