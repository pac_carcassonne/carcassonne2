#include "../Networking/Server/CarcassonneGame.h"
#include "../LoggingLibrary/Logging.h"

#include <fstream>

std::ofstream outputFile("gameLog.log", std::ios::out | std::ios::trunc);
Logger logger(outputFile);

int main(int argc, char* argv[])
{
	logger.Log("Game started...", Logger::Level::Info);

	CarcassonneGame game;

	return 0;
}