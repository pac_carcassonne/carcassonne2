#pragma once

#include "RendererManager.h"
#include "MenuGUI.h"
#include "../../Networking/Client/OnlineMenuGUI.h"
#include "../../Networking/Client/CreateServerGUI.h"
#include "../../Networking/Client/GameGUI.h"
#include "ResultsGUI.h"
#include "PlayersNameMenuGUI.h"

class ApplicationWindow
{
public:
	//Initialize the window
	ApplicationWindow(const char* title, int height = 720, int width = 1280, 
		int xpos = SDL_WINDOWPOS_CENTERED, int ypos = SDL_WINDOWPOS_CENTERED, Uint32 flags = 0);
	
	//Changes the content of GUI based on the current state of the aplication
	void Run();

	//Destroys the window
	void Close();

private:
	SDL_Window* m_window;
};

