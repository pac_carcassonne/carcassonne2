#pragma once

#include "ViewObject.h"
#include "SDL/SDL_image.h"

class ImageObject : public ViewObject
{
public:
	//create a visual object from an image
	ImageObject(const char* imageFileName,
		const int& height = 720, const int& width = 1280, const int& xpos = 0, const int& ypos = 0);
};

