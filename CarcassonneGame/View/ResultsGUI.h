#pragma once

#include "WindowContent.h"

class ResultsGUI : public WindowContent
{
public:
	//Initialize results GUI
	ResultsGUI();

	//run results GUI (infinite loop)
	ApplicationStatus Run() override;

private:
	ImageObject* m_backgroundTexture;
	ImageObject* m_continueButton;
};
