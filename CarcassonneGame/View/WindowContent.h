#pragma once

#include "ApplicationStatus.h"
#include "BoardView.h"
#include "TextObject.h"

#include <optional>
#include <tuple>

class WindowContent
{
protected:
	//TO DO: implement an infinite loop in derived classes
	virtual ApplicationStatus Run() = 0;

	//return false if close button was clicked or escape key pressed
	bool IsRunning();

	//return true if a mouse button was clicked
	std::optional<std::tuple<int, int>> MouseClick();
};