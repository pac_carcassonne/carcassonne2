#pragma once

#include "WindowContent.h"

class PlayersNameMenuGUI : public WindowContent
{
public:
	//Initialize players name menu GUI
	PlayersNameMenuGUI();

	//run players name menu GUI (infinite loop)
	ApplicationStatus Run() override;

private:
	ImageObject* m_backgroundTexture;
	ImageObject* m_playButton;
	TextObject* m_text;
};

