#pragma once

#include "ViewObject.h"
#include "SDL/SDL_ttf.h"

#include <fstream>

class TextObject : public ViewObject
{
public:
	//create a visual object from a text
	TextObject(const char* text, const int& height = 24, const int& width = 480,
		const int& xpos = 0, const int& ypos = 0);
};

