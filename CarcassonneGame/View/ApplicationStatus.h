#pragma once

enum class ApplicationStatus
{
	InMenu,
	InPlayersNameMenu,
	InOnlineMenu,
	InCreateServer,
	InJoinServer,
	InGame,
	InOnlineGame,
	InResults,
	Exit
};