#include "PlayersNameMenuGUI.h"

PlayersNameMenuGUI::PlayersNameMenuGUI()
{
	m_backgroundTexture = new ImageObject("assets/Wooden_Background.jpg");
	logger.Log("Menu backround texture created", Logger::Level::Info);

	m_playButton = new ImageObject("assets/play.png", 331, 460, 750, 350);
	logger.Log("play button texture created", Logger::Level::Info);

	m_text = new TextObject("Insert player name:");
}

ApplicationStatus PlayersNameMenuGUI::Run()
{
	while (IsRunning())
	{
		SDL_RenderClear(RendererManager::m_renderer);

		m_backgroundTexture->Render();
		m_playButton->Render();
		m_text->Render();

		SDL_RenderPresent(RendererManager::m_renderer);

		std::optional<std::tuple<int, int>> coordinates = MouseClick();
		if (coordinates)
		{
			int x, y;
			std::tie(x, y) = *coordinates;

			if (m_playButton->Clicked(x, y))
			{
				logger.Log("new game button was clicked", Logger::Level::Info);
				return ApplicationStatus::InGame;
			}
		}
	}

	return ApplicationStatus::Exit;
}
