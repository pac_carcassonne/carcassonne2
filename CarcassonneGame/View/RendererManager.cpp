#include "RendererManager.h"

SDL_Renderer* RendererManager::m_renderer = nullptr;

void RendererManager::InitializeRenderer(SDL_Window* window)
{
	m_renderer = SDL_CreateRenderer(window, -1, 0);
	logger.Log("Renderer created", Logger::Level::Info);
}

void RendererManager::DestroyRenderer()
{
	SDL_DestroyRenderer(m_renderer);
	logger.Log("Renderer destroyed", Logger::Level::Info);
}
