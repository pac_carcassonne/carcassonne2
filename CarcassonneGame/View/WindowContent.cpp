#include "WindowContent.h"

bool WindowContent::IsRunning()
{
	SDL_Event event;
	SDL_PollEvent(&event);

	switch (event.type)
	{
		case SDL_QUIT:
		{
			logger.Log("Exited game!", Logger::Level::Warning);

			return false;
		}

		case SDL_KEYDOWN:
		{
			switch (event.key.keysym.sym)
			{
				case SDLK_ESCAPE:
				{
					logger.Log("Exited game!", Logger::Level::Warning);

					return false;
				}
			}
		}
	}

	return true;
}

std::optional<std::tuple<int, int>> WindowContent::MouseClick()
{
	SDL_Event event;
	SDL_PollEvent(&event);

	if (event.type == SDL_MOUSEBUTTONDOWN)
	{
		logger.Log("Player pressed a button on his mouse!", Logger::Level::Info);

		return { {event.button.x, event.button.y} };
	}

	return {};
}
