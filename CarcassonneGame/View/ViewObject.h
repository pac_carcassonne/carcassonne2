#pragma once

#include "RendererManager.h"
#include "SDL/SDL.h"

class ViewObject
{
public:
	//update the position of the object on screen
	void Update(const int& xpos, const int& ypos);

	//render the object
	virtual void Render();

	//check if button was clicked
	bool Clicked(const int& x, const int& y);

protected:
	SDL_Texture* m_texture;
	SDL_Rect m_rectangle;
};