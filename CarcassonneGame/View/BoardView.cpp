#include "BoardView.h"

BoardView::BoardView(const char * imageFileName, const int & height, const int & width, const int & xpos, const int & ypos) :
	ImageObject(imageFileName, height, width, xpos, ypos)
{
	m_board.resize(7);

	for (std::vector<ImageObject*>& line : m_board)
		line.resize(9);

	m_board[3][4] = new ImageObject("assets/m.png", 90, 90, m_rectangle.x + 5 + 4 * 90, m_rectangle.y + 5 + 3 * 90);
}

void BoardView::Render()
{
	SDL_RenderCopy(RendererManager::m_renderer, m_texture, NULL, &m_rectangle);
	
	for (std::vector<ImageObject*>& line : m_board)
		for (ImageObject* tile : line)
			if (tile)
			{
				tile->Render();
			}
}

std::pair<int, int> BoardView::PlaceTile(int x, int y, char* name)
{
	int row;
	int column;

	row = (y - m_rectangle.y - 5) / 90;
	column = (x - m_rectangle.x - 5) / 90;

	m_board[row][column] = new ImageObject(name, 90, 90, m_rectangle.x + 5 + column * 90, m_rectangle.y + 5 + row * 90);

	return std::pair<int, int>(row, column);
}

void BoardView::PlaceTileFromString(std::string message)
{
	const char* string = message.c_str();

	char string2[2];
	strcpy(string2, string + 2);

	char tileImageName[20] = "assets/";
	std::string name = string2;
	const char* cpy = name.c_str();

	strcat(tileImageName, cpy);
	strcat(tileImageName, ".png");

	int x = string[0] - 48;
	int y = string[1] - 48;

	m_board[x][y] = new ImageObject(tileImageName, 90, 90, m_rectangle.x + 5 + y * 90, m_rectangle.y + 5 + x * 90);
}
