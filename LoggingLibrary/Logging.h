#pragma once

#ifdef  LOGGINGLIBRARY_EXPORTS
	#define LOGGING_API __declspec(dllexport)
#else
#define LOGGING_API __declspec(dllexport)
#endif //  LOGGINGLIBRARY_EXPORTS


#include <iostream>
#include <string>

class LOGGING_API Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};

public:
	Logger(std::ostream& outputStream, Level minimumLevel = Level::Info);

	void Log(const char * message, Level level);
	void Log(const std::string& message, Level level);

	void SetMinimumLogLevel(Level level);

private:
	std::ostream& m_outputStream;
	Level m_minimumLevel;
};