#include "Logging.h"

const char* LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	default:
		return "";
	}
}

Logger::Logger(std::ostream & outputStream, Logger::Level minimumLevel) :
	m_outputStream(outputStream),
	m_minimumLevel(minimumLevel)
{
}

void Logger::Log(const char * message, Level level)
{
	if (static_cast<int>(level) < static_cast<int>(m_minimumLevel))
		return;

	m_outputStream << "[" << LogLevelToString(level) << "] " << message << std::endl;
}

void Logger::Log(const std::string & message, Level level)
{
	this->Log(message.c_str(), level);
}

void Logger::SetMinimumLogLevel(Level level)
{
	this->m_minimumLevel = level;
}
