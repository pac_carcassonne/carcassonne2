#include "CarcassonneGame.h"

Server server;

CarcassonneGame::CarcassonneGame()
{
	application = new ApplicationWindow("CarcassonneGame");

	MenuGUI menu;
	
	switch (menu.Run())
	{
		case ApplicationStatus::InGame:
		{
			RunLocal();
			break;
		}
		case ApplicationStatus::InOnlineMenu:
		{
			RunOnline();
			break;
		}
		case ApplicationStatus::Exit:
		{
			application->Close();
		}
	}

}

void CarcassonneGame::RunLocal()
{
	Tile startingTile(Feature::City, Feature::Road, Feature::Road, Feature::Field, Feature::Road, "a");
	Board board(startingTile);
	Follower follower;
	int kNumberOfFollowers = 8;

	std::string playerName;
	std::string color = "";

	std::cout << "First player name: ";
	std::cin >> playerName;

	firstPlayer = new Player(playerName);

	std::cout << "First player choses a color: (Red, Green, Blue, Yellow, Black): ";

	while (true)
	{
		try
		{
			std::cin >> color;
			firstPlayer->SetColor(color);
			break;
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage;
		}
	}

	for (int index = 0; index < kNumberOfFollowers; ++index)
	{
		firstPlayer->SetFollowers(kNumberOfFollowers);
	}

	std::cout << "Second player name: ";
	std::cin >> playerName;

	secondPlayer = new Player(playerName);

	std::cout << "Second player chooses a color: (Red, Green, Blue, Yellow, Black): ";

	while (true)
	{
		try
		{
			std::cin >> color;
			secondPlayer->SetColor(color);
			if (secondPlayer->GetColor() == firstPlayer->GetColor())
				throw "pick other color: ";
			break;
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage;
		}
	}

	for (int index = 0; index < kNumberOfFollowers; ++index)
	{
		secondPlayer->SetFollowers(kNumberOfFollowers);
	}

	std::reference_wrapper<Player> pickingPlayer = *firstPlayer;
	std::reference_wrapper<Player> waitingPlayer = *secondPlayer;

	UnusedTiles unusedTiles;
	unusedTiles.Shuffle();

	Tile unusedTile = unusedTiles.GetTile();
	auto randomTile = std::make_shared<Tile>(unusedTile);
	std::shared_ptr<Tile> pickedTile(randomTile);

	GameGUI game;
	while (game.Update(*pickedTile) == ApplicationStatus::InGame)
	{ 
		unusedTile = unusedTiles.GetTile();

		randomTile = std::make_shared<Tile>(unusedTile);

		pickedTile = randomTile;
	}

	//std::swap(pickingPlayer, waitingPlayer);
}

void CarcassonneGame::RunOnline()
{
	OnlineMenuGUI onlineMenu;
	
	switch(onlineMenu.Run())
	{ 
		case ApplicationStatus::InCreateServer:
		{
			CreateServer();
			break;
		}
		case ApplicationStatus::InJoinServer:
		{
			JoinServer("player2 ready");
			Player2OnlineGame();
			break;
		}
		case ApplicationStatus::Exit:
		{
			application->Close();
		}
	}
	
}

void ServerManager()
{
	server.Initialize();
	server.CreateSocket();
	server.BindingSocket();
	server.ListeningOnSocket();
	server.AcceptConnection();
	server.SendData(1);
	server.SendData(2);
}

void CarcassonneGame::CreateServer()
{
	CreateServerGUI createServerGUI;

	std::thread createServer(ServerManager);

	JoinServer("player1 ready");

	std::string info;

	switch (createServerGUI.Run())
	{
		case ApplicationStatus::Exit :
			application->Close();
	}

	m_client.ReceiveData();

	OnlineGame();
}

void CarcassonneGame::JoinServer(std::string message)
{
	try
	{
		std::ifstream ipAdress("..//serverIP.txt");

		std::string ip;
		ipAdress >> ip;

		m_client.SetIP(ip);

		m_client.InitializeWinsocket();
		m_client.CreateSocket();
		m_client.ConnectToServer();
		m_client.SendData(message);
	}
	catch (const std::exception& currentException)
	{
		currentException.what();
	}
}

void CarcassonneGame::OnlineGame()
{
	Tile startingTile(Feature::City, Feature::Road, Feature::Road, Feature::Field, Feature::Road, "a");
	Board board(startingTile);

	UnusedTiles unusedTiles;
	unusedTiles.Shuffle();

	Tile unusedTile = unusedTiles.GetTile();
	auto randomTile = std::make_shared<Tile>(unusedTile);
	std::shared_ptr<Tile> pickedTile(randomTile);

	GameGUI game;

	std::string message2 = "34m";

	while (true)
	{
		std::string message = game.YourTurnOnline(*pickedTile, message2);

		if (message == "exit")
			break;
		else
		{
			m_client.SendData(message);
			server.SendData(1);
		}

		unusedTile = unusedTiles.GetTile();
		randomTile = std::make_shared<Tile>(unusedTile);
		pickedTile = randomTile;

		m_client.SendData(pickedTile.get()->GetName());
		server.SendData(1);

		message2 = "no message";

		while (message2 == "no message")
		{
			switch (game.WaitOpponent())
			{
				case ApplicationStatus::InGame:
				{
					server.SendData(2);
					message = m_client.ReceiveData().c_str();
					break;
				}
				case ApplicationStatus::Exit:
				{
					application->Close();
					break;
				}
			}
		}

		if (message2 == "no message")
			break;
	}

	m_client.ShutdownSending();
	m_client.Cleanup();

	application->Close();
}

void CarcassonneGame::Player2OnlineGame()
{
	m_client.ReceiveData();

	GameGUI game;

	std::string message2 = "34m";

	game.WaitOpponent();

	m_client.ReceiveData();

	m_client.ReceiveData();

	while (true)
	{
		game.WaitOpponent();
	}
	m_client.Cleanup();

	application->Close();
}