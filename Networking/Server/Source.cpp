#include "Server.h"

int main()
{
	Server server;

	server.Initialize();
		
	server.CreateSocket();

	server.BindingSocket();

	server.ListeningOnSocket();

	server.AcceptConnection();

	server.SendData(1);

	server.SendData(2);

	//server.Disconnect();
	
	//server.Cleanup();
	
	return 0;
}