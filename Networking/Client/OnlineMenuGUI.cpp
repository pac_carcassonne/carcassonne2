#include "OnlineMenuGUI.h"

OnlineMenuGUI::OnlineMenuGUI()
{
	m_backgroundTexture = new ImageObject("assets/menu_background.jpeg");
	logger.Log("Menu backround texture created", Logger::Level::Info);

	m_createServerButton = new ImageObject("assets/create_server.jpeg", 64, 250, 250, 515);
	logger.Log("new game button texture created", Logger::Level::Info);

	m_joinServerButton = new ImageObject("assets/join_server.jpeg", 64, 250, 780, 515);
	logger.Log("new game button texture created", Logger::Level::Info);
}

ApplicationStatus OnlineMenuGUI::Run()
{
	while (IsRunning())
	{
		SDL_RenderClear(RendererManager::m_renderer);

		m_backgroundTexture->Render();
		m_createServerButton->Render();
		m_joinServerButton->Render();

		SDL_RenderPresent(RendererManager::m_renderer);

		std::optional<std::tuple<int, int>> coordinates = MouseClick();
		if (coordinates)
		{
			int x, y;
			std::tie(x, y) = *coordinates;

			if (m_createServerButton->Clicked(x, y))
			{
				logger.Log("create server button was clicked", Logger::Level::Info);
				return ApplicationStatus::InCreateServer;
			}

			if (m_joinServerButton->Clicked(x, y))
			{
				logger.Log("join server button was clicked", Logger::Level::Info);
				return ApplicationStatus::InJoinServer;
			}
		}
	}

	return ApplicationStatus::Exit;
}
