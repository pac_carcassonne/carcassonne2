#include "GameGUI.h"

GameGUI::GameGUI()
{
	m_backgroundTexture = new ImageObject("assets/Wooden_Background.jpg");
	logger.Log("backround texture created", Logger::Level::Info);

	m_board = new BoardView("assets/grid.png");
	logger.Log("board texture created", Logger::Level::Info);
}

ApplicationStatus GameGUI::Run()
{
	return ApplicationStatus();
}

ApplicationStatus GameGUI::Update(const Tile& tile)
{
	char tileImageName[20] = "assets/";
	std::string name = tile.GetName();
	const char* cpy = name.c_str();

	strcat(tileImageName, cpy);
	strcat(tileImageName, ".png");

	ImageObject currentTile(tileImageName, 90, 90, 165, 300);

	ImageObject currentPlayer("assets/your_turn.jpeg", 100, 300, 60, 40);

	while (IsRunning())
	{
		SDL_RenderClear(RendererManager::m_renderer);

		m_backgroundTexture->Render();
		m_board->Render();
		currentPlayer.Render();
		currentTile.Render();

		SDL_RenderPresent(RendererManager::m_renderer);

		std::optional<std::tuple<int, int>> coordinates = MouseClick();
		if (coordinates)
		{
			int x, y;
			std::tie(x, y) = *coordinates;

			if (m_board->Clicked(x, y))
			{
				m_board->PlaceTile(x, y, tileImageName);

				return ApplicationStatus::InGame;
			}
		}
	}

	return ApplicationStatus::Exit;
}

std::string GameGUI::YourTurnOnline(const Tile& tile, std::string message)
{
	m_board->PlaceTileFromString(message);

	char tileImageName[20] = "assets/";
	std::string name = tile.GetName();
	const char* cpy = name.c_str();

	strcat(tileImageName, cpy);
	strcat(tileImageName, ".png");

	ImageObject currentTile(tileImageName, 90, 90, 165, 300);

	ImageObject currentPlayer("assets/your_turn.jpeg", 100, 300, 60, 40);

	while (IsRunning())
	{
		SDL_RenderClear(RendererManager::m_renderer);

		m_backgroundTexture->Render();
		m_board->Render();
		currentPlayer.Render();
		currentTile.Render();

		SDL_RenderPresent(RendererManager::m_renderer);

		std::optional<std::tuple<int, int>> coordinates = MouseClick();
		if (coordinates)
		{
			int x, y;
			std::tie(x, y) = *coordinates;

			if (m_board->Clicked(x, y))
			{
				std::pair<int, int> place = m_board->PlaceTile(x, y, tileImageName);

				char xpos[2];
				char ypos[2];

				_itoa(place.first, xpos, 10);
				_itoa(place.second, ypos, 10);

				char placedTile[4] = "";
				strcat(placedTile, xpos);
				strcat(placedTile, ypos);
				strcat(placedTile, cpy);

				return placedTile;
			}
		}
	}

	return "exit";
}

ApplicationStatus GameGUI::WaitOpponent()
{
	ImageObject currentPlayer("assets/not_your_turn.jpeg", 100, 300, 60, 40);

	if (IsRunning())
	{
		SDL_RenderClear(RendererManager::m_renderer);

		m_backgroundTexture->Render();
		m_board->Render();
		currentPlayer.Render();

		SDL_RenderPresent(RendererManager::m_renderer);

		return ApplicationStatus::InGame;
	}

	return ApplicationStatus::Exit;
}

void GameGUI::InitPlayerColor(const Player::Color& playerColor)
{
	switch (playerColor)
	{
		case Player::Color::Red:
		{
			m_currentPlayerColor = new ImageObject("assets/red.png", 100, 300, 60, 40);
			break;
		}

		case Player::Color::Green:
		{
			m_currentPlayerColor = new ImageObject("assets/green.png", 100, 300, 60, 40);
			break;
		}

		case Player::Color::Blue:
		{
			m_currentPlayerColor = new ImageObject("assets/blue.png", 100, 300, 60, 40);
			break;
		}

		case Player::Color::Yellow:
		{
			m_currentPlayerColor = new ImageObject("assets/yellow.png", 100, 300, 60, 40);
			break;
		}

		case Player::Color::Black:
		{
			m_currentPlayerColor = new ImageObject("assets/black.png", 100, 300, 60, 40);
		}
	}

	logger.Log("player color texture created", Logger::Level::Info);
}
