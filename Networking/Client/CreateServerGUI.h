#pragma once

#include "WindowContent.h"

class CreateServerGUI : public WindowContent
{
public:
	//Initialize menu GUI
	CreateServerGUI();

	//run menu GUI (infinite loop)
	ApplicationStatus Run() override;

private:
	ImageObject* m_backgroundTexture;
};